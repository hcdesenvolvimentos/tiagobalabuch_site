<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'hcdesenv_tiago_site');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'hcdesenv');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'hc@hc1468');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ' u L$_%$f0Z3?0U/`Emgk27D2RD9b]){;jQN#c0c?+sfT1(PpH^r_Dw+jrNB;OkD');
define('SECURE_AUTH_KEY',  '|5,f:)vF1l)&3B2hE8eCL:DJA i8_K0bj7G@/Z|};^P2f9LHf+8,JDgIrPUL+z0w');
define('LOGGED_IN_KEY',    'j%*n&|h1i[^J){Zzrw&B:0McKG<hy[_h5uk:cP#,w.Y(mt$GL,8}2RDqhfI4Lid)');
define('NONCE_KEY',        '6/s(Cgv(40s]z8Ubyt5MWMSc^LXu<+#>G(AYf-63]k|N[QJX8+xk I~vUBKz8Lah');
define('AUTH_SALT',        'Ys0{ 3$pZ~Z-Skrkglcy%lbF;$;j(z;cV=Z)+m L#a=/cRHeKps7}F8<.o`Rc[=@');
define('SECURE_AUTH_SALT', 'a?a`q*qT5CO-2jlJ:ewEyt^X/F>2d1eU6b4O&{e%otLFf_deN?^0e)+vGFJW2?{d');
define('LOGGED_IN_SALT',   'r|Ev+(8 :4[ALa2M]OIh]1-1,@~ hb1GM,.Dz[/-A8][+e_mpP! #3d|!y-KP8(T');
define('NONCE_SALT',       'N3,Ny)R (!M xp``blL3Z_rA#2(F8UAv>%R6]#pr>+.<@5M7m4721@ZDv!Gh;U_`');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'tb_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
