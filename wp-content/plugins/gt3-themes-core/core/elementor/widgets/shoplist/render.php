<?php

if(!defined('ABSPATH')) {
	exit;
}

/** @var \ElementorModal\Widgets\GT3_Core_Elementor_Widget_ShopList $widget */

/*$settings = array(
	'flip_type'    => 'left',
	'flip_style'   => true,
	'link'         => array( 'url' => '#', 'is_external' => false, 'nofollow' => false, ),
	'link_title'   => '',
	'index_number' => '',
	'image'        => array( 'url' => Utils::get_placeholder_image_src(), ),
	'title'        => '',
	'subtitle'     => '',
	'content_text' => '',
);*/

//$settings = wp_parse_args($widget->get_settings(), $settings);

$settings = $widget->get_settings();

if (!empty($settings['scroll_anim'])) {
    wp_enqueue_script('gt3-appear');
}

// Woo Category render
$gt3_tax_query = '';
if (!empty($settings['woo_category'])) {
    $categories    = explode(',', $settings['woo_category']);
    $gt3_tax_query = array(
        array(
            'taxonomy' => 'product_cat',
            'terms'    => $categories,
            'field'    => 'slug',
            'operator' => 'IN'
        )
    );
}

$product_visibility_terms  = wc_get_product_visibility_term_ids();
$product_visibility_not_in = $product_visibility_terms['exclude-from-catalog'];
if ('yes' === get_option('woocommerce_hide_out_of_stock_items')) {
    $gt3_tax_query[] = array(
        'taxonomy' => 'product_visibility',
        'field'    => 'name',
        'terms'    => array('outofstock', 'exclude-from-catalog'),
        'operator' => 'NOT IN',
    );
} else {
    $gt3_tax_query[] = array(
        'taxonomy' => 'product_visibility',
        'field'    => 'term_taxonomy_id',
        'terms'    => $product_visibility_not_in,
        'operator' => 'NOT IN',
    );
}

// Select filter sortby
$orderby = $settings['orderby'];
$order   = $settings['order'];
if (isset($_GET['orderby'])) {
    $orderby_value = explode('-', $_GET['orderby']);
    $orderby       = esc_attr($orderby_value[0]);
    $order         = !empty($orderby_value[1]) ? $orderby_value[1] : $order;
    if ($_GET['orderby'] == 'price') {
        $order = 'ASC';
    }
}

$ordering_args = WC()->query->get_catalog_ordering_args($orderby, $order);
$meta_query    = WC()->query->get_meta_query();

// Pagination setup
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

// Select how many products to show
//$per_page = $settings['prod_per_row'] * $settings['rows_per_page'];
$per_page = $settings['prod_per_page'];
if (isset($_GET['show_prod'])) $per_page = $_GET['show_prod'];

if (class_exists('GT3_GridList_WOO')) {
    if ($settings['grid_style'] === 'grid_default hover_default' ||
        $settings['grid_style'] === 'grid_default_woo hover_default') {
        \GT3_GridList_WOO::gt3_enqueue_scripts();
    }
}

$args = array(
    'post_type'           => 'product',
    'post_status'         => 'publish',
    'ignore_sticky_posts' => 1,
    'orderby'             => $ordering_args['orderby'],
    'order'               => $ordering_args['order'],
    'meta_key'            => $ordering_args['meta_key'],
    'posts_per_page'      => $per_page,
    'paged'               => $paged,
    'meta_query'          => $meta_query,
    'tax_query'           => $gt3_tax_query
);

global $products;
wc_set_loop_prop('columns', $settings['prod_per_row']);
$products = new \WP_Query(apply_filters('woocommerce_shortcode_products_query', $args, $settings));

$settings['grid_style_rended'] = '';
$set_grid_style                = preg_split("/ /", $settings['grid_style']);
if (is_array($set_grid_style)) {
    $settings['grid_style_rended'] = array(
        'grid'  => $set_grid_style[0],
        'hover' => $set_grid_style[1]
    );
}

$preg_match_masonry = (bool)preg_match('/grid_masonry/i', $settings['grid_style']);
$preg_match_packery = (bool)preg_match('/grid_packery/i', $settings['grid_style']);

$shop_list_v_position = !empty($settings['shop_list_v_position']) ? $settings['shop_list_v_position'] : 'top';

$products_class = '';
$products_class .= !empty($settings['grid_style']) ? ' ' . $settings['grid_style'] : '';
$products_class .= $preg_match_masonry ? ' shop_grid_masonry' : '';
$products_class .= $preg_match_packery ? ' shop_grid_masonry shop_grid_packery' : '';

if ('grid_default' == $settings['grid_style_rended']['grid'] ||
    'grid_default_woo' == $settings['grid_style_rended']['grid']) {
    $products_class .= !empty($settings['equal_height']) ? ' shop_list_equal_height' : ' shop_list_position-' . $shop_list_v_position;
}
$products_class .= !empty($settings['products_shadow']) ? ' shadow' : '';
$products_class .= !empty($settings['infinity_scroll']) ? ' gt3_infinity_scroll' : '';

$gap = !empty($settings['grid_gap']) && is_array($settings['grid_gap']) ? 'width:'.$settings['grid_gap']['size']. $settings['grid_gap']['unit'] : '';

/*$infinity_scroll = '{ "path": ".next.page-numbers", "append": ".woocommerce > .products > .product", "history": false, "hideNav": ".woocommerce-pagination"';
$infinity_scroll .= $preg_match_masonry ? ', "outlayer": "msnry"' : '';
$infinity_scroll .= $preg_match_packery ? ', "outlayer": "pckry"' : '';
$infinity_scroll .= '}';*/

if ($preg_match_masonry){
    $infinity_scroll = 'msnry';
}elseif($preg_match_packery){
    $infinity_scroll = 'pckry';
}else{
    $infinity_scroll = '';
}

ob_start();
if ($products->have_posts()) {
    if ((bool)$settings['shop_grid_list'] ||
        $settings['dropdown_prod_per_page'] == 'top' ||
        $settings['dropdown_prod_per_page'] == 'bottom_top' ||
        $settings['dropdown_prod_orderby'] == 'top' ||
        $settings['dropdown_prod_orderby'] == 'bottom_top' ||
        $settings['pagination'] == 'top' ||
        $settings['pagination'] == 'bottom_top') {

        ?>
        <!--                <div class="gt3-products-header --><?php //echo esc_attr($products_class); ?><!--">-->
        <div class="gt3-products-header">
            <?php
            if (class_exists('GT3_GridList_WOO') && (bool)$settings['shop_grid_list'] &&
                ('grid_default' == $settings['grid_style_rended']['grid'] ||
                    'grid_default_woo' == $settings['grid_style_rended']['grid'])) {
                \GT3_GridList_WOO::toggle_button();
            }
            if (($settings['dropdown_prod_per_page'] == 'top' || $settings['dropdown_prod_per_page'] == 'bottom_top') && !(bool)$settings['infinity_scroll']) {
                gt3_get_woo_template('loop/product-show'); // Product show
            }
            if ($settings['dropdown_prod_orderby'] == 'top' || $settings['dropdown_prod_orderby'] == 'bottom_top') {
                gt3_get_woo_template('loop/orderby'); // Orderby
            }

            if (($settings['pagination'] == 'top' || $settings['pagination'] == 'bottom_top') && !(bool)$settings['infinity_scroll']) {
                gt3_get_woo_template('pagination');
            }
            ?>
        </div> <!-- gt3-products-header -->
        <?php
    } ?>
    <!--<ul class="products <?php /*echo esc_attr($products_class); */?>" data-infinite-scroll='<?php /*echo esc_attr($infinity_scroll) */?>'>-->
    <ul class="products <?php echo esc_attr($products_class); ?>" data-gt3-infinite-scroll='<?php echo esc_attr($infinity_scroll) ?>'>
        <?php
        do_action('gt3_woocommerce_before_shop_loop'); // gt3_archive_product_hooks - 150

        switch ($settings['prod_per_row']) {
            case '2':
                $packery_array = array('gt3_large_h_rect', 'gt3_default_rect', 'gt3_default_rect', 'gt3_default_rect', 'gt3_large_h_rect', 'gt3_default_rect');
                break;
            case '3':
                $packery_array = array('gt3_default_rect', 'gt3_large_h_rect', 'gt3_default_rect', 'gt3_large_h_rect', 'gt3_default_rect', 'gt3_large_rect', 'gt3_default_rect', 'gt3_default_rect', 'gt3_large_h_rect', 'gt3_default_rect', 'gt3_default_rect', 'gt3_large_h_rect', 'gt3_large_rect', 'gt3_default_rect');
                break;
            case '4':
                $packery_array = array('gt3_default_rect', 'gt3_default_rect', 'gt3_large_rect', 'gt3_default_rect', 'gt3_default_rect', 'gt3_large_rect', 'gt3_default_rect', 'gt3_default_rect', 'gt3_default_rect', 'gt3_default_rect');
                break;
            case '5':
                $packery_array = array('gt3_default_rect', 'gt3_default_rect', 'gt3_default_rect', 'gt3_large_rect', 'gt3_large_h_rect', 'gt3_large_h_rect', 'gt3_large_h_rect', 'gt3_default_rect', 'gt3_default_rect', 'gt3_large_rect', 'gt3_default_rect', 'gt3_large_h_rect', 'gt3_large_h_rect', 'gt3_default_rect');
                break;
            case '6':
                $packery_array = array('gt3_default_rect', 'gt3_default_rect', 'gt3_large_rect', 'gt3_default_rect', 'gt3_default_rect', 'gt3_large_rect', 'gt3_default_rect', 'gt3_default_rect', 'gt3_default_rect', 'gt3_default_rect');
                // exactly as in case 4
                break;
            default:
                $packery_array = array('gt3_large_h_rect', 'gt3_default_rect', 'gt3_default_rect', 'gt3_default_rect', 'gt3_large_h_rect', 'gt3_default_rect');
                break;
        }
        $packery_array = apply_filters( 'gt3_core_packery_array', $packery_array );
        $packery_count = count($packery_array);
        ?>
        <li class="product-gutter-size" style="<?php echo esc_attr($gap); ?>"></li>
        <li class="product-default-width gt3_product_width"></li>

        <?php
        while ($products->have_posts()) {
            $products->the_post();

            if ('grid_masonry' == $settings['grid_style_rended']['grid']) {
                $settings['thumbnail_dim'] = 'post-thumbnail';
            } elseif ('grid_packery' == $settings['grid_style_rended']['grid']) {
                $number_pos                = !isset($number_pos) ? 1 : $number_pos;
                $settings['thumbnail_dim'] = $packery_array[$number_pos - 1];

                $number_pos = $number_pos == $packery_count ? 1 : ++$number_pos;
            } elseif ('grid_masonry_custom' == $settings['grid_style_rended']['grid']) {
                $gt3_masonry_image_size = get_post_meta(get_the_ID(), 'mb_img_size_masonry', true);
                switch ($gt3_masonry_image_size) {
                    case 'large_h_rect':
//                        $settings['thumbnail_dim'] = 'gt3_912x730';
                        $settings['thumbnail_dim'] = 'gt3_large_h_rect';
                        break;
                    case 'large_v_rect':
//                        $settings['thumbnail_dim'] = 'gt3_442x730';
                        $settings['thumbnail_dim'] = 'gt3_large_v_rect';
                        break;
                    case 'large_rect':
//                        $settings['thumbnail_dim'] = 'gt3_442x730';
                        $settings['thumbnail_dim'] = 'gt3_large_rect';
                        break;
                    default:
//                        $settings['thumbnail_dim'] = 'gt3_442x350';
                        $settings['thumbnail_dim'] = 'gt3_default_rect';
                        break;
                }
            } elseif ('grid_default' == $settings['grid_style_rended']['grid']) {
                $settings['thumbnail_dim'] = 'gt3_catalog_images';
            } elseif ('grid_default_woo' == $settings['grid_style_rended']['grid']) {
                $settings['thumbnail_dim'] = 'shop_catalog';
            } else {
//                $settings['thumbnail_dim'] = 'gt3_912x730';
                $settings['thumbnail_dim'] = 'gt3_default_rect';
            }
            do_action('woocommerce_shop_loop');

            gt3_get_woo_template('content-product', $settings); // Content output

        } // end of the loop. ?>

        <?php if ((bool)$settings['infinity_scroll']){ ?>
            <li class="bubblingG visible">
                <span id="bubblingG_1"></span>
                <span id="bubblingG_2"></span>
                <span id="bubblingG_3"></span>
            </li>
        <?php } ?>

    </ul>

    <?php

    if ($settings['pagination'] == "bottom_top" ||
        $settings['pagination'] == "bottom" ||
        $settings['dropdown_prod_per_page'] == 'bottom' ||
        $settings['dropdown_prod_per_page'] == 'bottom_top' ||
        $settings['dropdown_prod_orderby'] == 'bottom' ||
        $settings['dropdown_prod_orderby'] == 'bottom_top') {
        ?>
        <div class="gt3-products-bottom">
            <?php
            if (($settings['pagination'] == "bottom_top" || $settings['pagination'] == "bottom") &&
                $products->max_num_pages > 1 && !(bool)$settings['infinity_scroll']) {
                gt3_get_woo_template('pagination');

            }
            if (($settings['dropdown_prod_per_page'] == 'bottom' || $settings['dropdown_prod_per_page'] == 'bottom_top') && !(bool)$settings['infinity_scroll']) {
                gt3_get_woo_template('loop/product-show'); // Product show
            }
            if ($settings['dropdown_prod_orderby'] == 'bottom' || $settings['dropdown_prod_orderby'] == 'bottom_top') {
                gt3_get_woo_template('loop/orderby'); // Orderby
            }
            ?>
        </div>
        <?php
    }
    if ((bool)$settings['infinity_scroll']) {
        gt3_get_woo_template('pagination');
    }
}
wp_reset_postdata();

$columns = !empty($settings['prod_per_row']) ? $settings['prod_per_row'] : 4;

echo '<div class="woocommerce gt3_theme_core gt3-shop-list columns-' . esc_attr($columns) . '">' . ob_get_clean() . '</div>';




