<?php

if (!defined('ABSPATH')) {
    exit;
}

use Elementor\Controls_Manager;


/** @var \ElementorModal\Widgets\GT3_Core_Elementor_Widget_ShopList $widget */

$widget->start_controls_section(
    'general',
    array(
        'label' => esc_html__('General', 'gt3_themes_core'),
    )
);

$widget->add_control(
    'hidden_update',
    array(
        'type' => Controls_Manager::HIDDEN,
    )
);

$widget->add_control(
    'woo_category',
    array(
        'label'       => esc_html__('Product Category', 'gt3_themes_core'),
        'description' => esc_html__('Leave an empty if you want to display all categories', 'gt3_themes_core'),
        'type'        => Controls_Manager::SELECT2,
        'default'     => '',
        'multiple'    => true,
        'options'     => $widget->get_woo_category(),
    )
);

$widget->add_control(
    'prod_per_row',
    array(
        'label'       => esc_html__('Products Per Row', 'gt3_themes_core'),
        'description' => esc_html__('How many products should be shown per row?', 'gt3_themes_core'),
        'type'        => Controls_Manager::SELECT,
        'options'     => array(
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
        ),
        'default'     => 3,
    )
);

$widget->add_control(
    'prod_per_page',
    array(
        'label'       => esc_html__('Prod Per Page', 'gt3_themes_core'),
        'description' => esc_html__('How many products should be shown per page?', 'gt3_themes_core'),
        'type'        => Controls_Manager::NUMBER,
        'min'         => 1,
        'step'        => 1,
        'default'     => 12,
    )
);

$widget->add_control(
    'infinity_scroll',
    array(
        'label' => esc_html__('Activate Infinity Scroll?', 'gt3_themes_core'),
        'type'  => Controls_Manager::SWITCHER,
    )
);

$widget->add_control(
    'orderby',
    array(
        'label'       => esc_html__('Order by', 'gt3_themes_core'),
        'description' => esc_html__('Select how to sort retrieved products', 'gt3_themes_core'),
        'type'        => Controls_Manager::SELECT,
        'options'     => array(
            'date'          => esc_html__('Date', 'gt3_themes_core'),
            'ID'            => esc_html__('ID', 'gt3_themes_core'),
            'author'        => esc_html__('Author', 'gt3_themes_core'),
            'modified'      => esc_html__('Modified', 'gt3_themes_core'),
            'rand'          => esc_html__('Random', 'gt3_themes_core'),
            'comment_count' => esc_html__('Comment count', 'gt3_themes_core'),
            'menu_order'    => esc_html__('Menu Order', 'gt3_themes_core'),
        ),
        'default'     => 'menu_order',
    )
);

$widget->add_control(
    'order',
    array(
        'label'       => esc_html__('Order way', 'gt3_themes_core'),
        'description' => esc_html__('Designates the ascending or descending order', 'gt3_themes_core'),
        'type'        => Controls_Manager::SELECT,
        'options'     => array(
            'DESC' => esc_html__('Descending', 'gt3_themes_core'),
            'ASC'  => esc_html__('Ascending', 'gt3_themes_core'),
        ),
        'default'     => 'DESC',
    )
);

$widget->end_controls_section();

// Tab Style
$widget->start_controls_section(
    'style',
    array(
        'label' => esc_html__('Shop Styles', 'gt3_themes_core'),
        'tab'   => Controls_Manager::TAB_STYLE
    )
);

$widget->add_control(
    'grid_style',
    array(
        'label'       => esc_html__('Grid Style', 'gt3_themes_core'),
        'type'        => Controls_Manager::SELECT,
        'label_block' => true,
        'groups'      => array(
            'Group 1' => array(
                'label'   => esc_html__('Default GT3', 'gt3_themes_core'),
                'options' => array(
                    'grid_default hover_default' => esc_html__('GT3 - Default Overlay', 'gt3_themes_core'),
                    'grid_default hover_bottom'  => esc_html__('GT3 - Bottom Title Overlay', 'gt3_themes_core'),
                    'grid_default hover_center'  => esc_html__('GT3 - Center Title Overlay', 'gt3_themes_core'),
                )
            ),
            'Group 2' => array(
                'label'   => esc_html__('Default WooCommerce', 'gt3_themes_core'),
                'options' => array(
                    'grid_default_woo hover_default' => esc_html__('Woo - Default Overlay', 'gt3_themes_core'),
                    'grid_default_woo hover_bottom'  => esc_html__('Woo - Bottom Title Overlay', 'gt3_themes_core'),
                    'grid_default_woo hover_center'  => esc_html__('Woo - Center Title Overlay', 'gt3_themes_core'),
                )
            ),
            'Group 3' => array(
                'label'   => esc_html__('Packery', 'gt3_themes_core'),
                'options' => array(
                    'grid_packery hover_bottom' => esc_html__('Packery - Bottom Title Overlay', 'gt3_themes_core'),
                    'grid_packery hover_center' => esc_html__('Packery - Center Title Overlay', 'gt3_themes_core'),
                )
            ),
            'Group 4' => array(
                'label'   => esc_html__('Masonry', 'gt3_themes_core'),
                'options' => array(
                    'grid_masonry hover_default' => esc_html__('Masonry - Default Overlay', 'gt3_themes_core'),
                    'grid_masonry hover_bottom'  => esc_html__('Masonry - Bottom Title Overlay', 'gt3_themes_core'),
                    'grid_masonry hover_center'  => esc_html__('Masonry - Center Title Overlay', 'gt3_themes_core'),
                )
            ),
            'Group 5' => array(
                'label'   => esc_html__('Custom Masonry', 'gt3_themes_core'),
                'options' => array(
                    'grid_masonry_custom hover_default' => esc_html__('Custom Masonry - Default Overlay', 'gt3_themes_core'),
                    'grid_masonry_custom hover_bottom'  => esc_html__('Custom Masonry - Bottom Title Overlay', 'gt3_themes_core'),
                    'grid_masonry_custom hover_center'  => esc_html__('Custom Masonry - Center Title Overlay', 'gt3_themes_core'),
                )
            ),
        ),
        'default'     => 'grid_default hover_default'
    )
);

$widget->add_control(
    'image_size',
    array(
        'label'     => esc_html__('Image size', 'gt3_themes_core'),
        'type'      => Controls_Manager::SELECT,
        'options'   => array(
            'cover'   => esc_html__('Cover', 'gt3_themes_core'),
            'contain' => esc_html__('Contain', 'gt3_themes_core'),
        ),
        'default'   => 'cover',
        'condition' => array(
            'grid_style' => array(
                'grid_packery hover_bottom',
                'grid_packery hover_center',
            ),
        ),
        'selectors' => array(
            '{{WRAPPER}} .gt3-product__packery-thumb' => 'background-size: {{VALUE}};',
        ),
    )
);

$widget->add_control(
    'grid_gap',
    array(
        'label'          => esc_html__('Grid Gap', 'gt3_themes_core'),
        'type'           => Controls_Manager::SLIDER,
        'size_units'     => ['px', '%'],
        'range'          => [
            'px' => [
                'min' => 0,
                'max' => 50,
            ],
            '%'  => [
                'min' => 0,
                'max' => 10,
            ],
        ],
        'default'        => [
            'size' => 30,
            'unit' => 'px',
        ],
        'tablet_default' => [
            'size' => 20,
            'unit' => 'px',
        ],
        'mobile_default' => [
            'size' => 15,
            'unit' => 'px',
        ],
        'selectors'      => array(
            '{{WRAPPER}} li.product' => 'margin-right: {{SIZE}}{{UNIT}};margin-bottom: {{SIZE}}{{UNIT}};',

            '{{WRAPPER}} .woocommerce.columns-2 ul.products li.gt3_product_width' => 'width: calc(49.95% - 1/2*{{SIZE}}{{UNIT}});',
            '{{WRAPPER}} .woocommerce.columns-3 ul.products li.gt3_product_width'  => 'width: calc(33.3% - 2/3*{{SIZE}}{{UNIT}});',
            '{{WRAPPER}} .woocommerce.columns-4 ul.products li.gt3_product_width'  => 'width: calc(24.95% - 3/4*{{SIZE}}{{UNIT}});',
            '{{WRAPPER}} .woocommerce.columns-5 ul.products li.gt3_product_width'  => 'width: calc(19.95% - 4/5*{{SIZE}}{{UNIT}});',
            '{{WRAPPER}} .woocommerce.columns-6 ul.products li.gt3_product_width'  => 'width: calc(16.6% - 5/6*{{SIZE}}{{UNIT}});',

            '{{WRAPPER}} .woocommerce.columns-2 ul.products li.gt3_product_width.large,
            {{WRAPPER}} .woocommerce.columns-2 ul.products li.gt3_product_width.large_horisontal' => 'width: 100%;',
            '{{WRAPPER}} .woocommerce.columns-3 ul.products li.gt3_product_width.large,
            {{WRAPPER}} .woocommerce.columns-3 ul.products li.gt3_product_width.large_horisontal'  => 'width: calc(66.6% - 1/3*{{SIZE}}{{UNIT}});',
            '{{WRAPPER}} .woocommerce.columns-4 ul.products li.gt3_product_width.large,
            {{WRAPPER}} .woocommerce.columns-4 ul.products li.gt3_product_width.large_horisontal'  => 'width: calc(49.95% - 1/2*{{SIZE}}{{UNIT}});',
            '{{WRAPPER}} .woocommerce.columns-5 ul.products li.gt3_product_width.large,
            {{WRAPPER}} .woocommerce.columns-5 ul.products li.gt3_product_width.large_horisontal'  => 'width: calc(39.95% - 3/5*{{SIZE}}{{UNIT}});',
            '{{WRAPPER}} .woocommerce.columns-6 ul.products li.gt3_product_width.large,
            {{WRAPPER}} .woocommerce.columns-6 ul.products li.gt3_product_width.large_horisontal'  => 'width: calc(33.3% - 2/3*{{SIZE}}{{UNIT}});',
        ),
    )
);

$widget->add_control(
    'hidden_update_2',
    array(
        'type' => Controls_Manager::HIDDEN,
    )
);
$widget->add_control(
    'update_button',
    array(
        'type'        => Controls_Manager::BUTTON,
        'text'        => esc_html__('Update', 'gt3_themes_core'),
        'label'       => esc_html__('Update Widget', 'gt3_themes_core'),
        'description' => esc_html__('Click this button to manual update widget render.', 'gt3_themes_core'),
        'button_type' => 'success',
        'event'       => 'update_widget',
        'condition'   => array(
            'grid_style!' => array(
                'grid_default hover_default',
                'grid_default hover_bottom',
                'grid_default hover_center',
                'grid_default_woo hover_default',
                'grid_default_woo hover_bottom',
                'grid_default_woo hover_center'
            ),
        )
    )
);

$widget->add_control(
    'equal_height',
    array(
        'label'       => esc_html__('Equal height', 'gt3_themes_core'),
        'description' => esc_html__('Products will be set to equal height', 'gt3_themes_core'),
        'type'        => Controls_Manager::SWITCHER,
        'condition'   => array(
            'grid_style' => array(
                'grid_default hover_default',
                'grid_default hover_bottom',
                'grid_default hover_center',
                'grid_default_woo hover_default',
                'grid_default_woo hover_bottom',
                'grid_default_woo hover_center'
            ),
        )
    )
);

$widget->add_control(
    'shop_list_v_position',
    array(
        'label'       => esc_html__('Select vertical position for these products', 'gt3_themes_core'),
        'description' => esc_html__('Note: This option is relevant if Catalog Images has different height and Crop is inactive. See "Theme Options > Shop > Products Page"', 'gt3_themes_core'),
        'type'        => Controls_Manager::SELECT,
        'options'     => array(
            'top'    => esc_html__('Top', 'gt3_themes_core'),
            'center' => esc_html__('Center', 'gt3_themes_core'),
            'bottom' => esc_html__('Bottom', 'gt3_themes_core'),
        ),
        'default'     => 'top',
        'condition'   => array(
            'grid_style'    => array(
                'grid_default hover_default',
                'grid_default hover_bottom',
                'grid_default hover_center',
                'grid_default_woo hover_default',
                'grid_default_woo hover_bottom',
                'grid_default_woo hover_center'
            ),
            'equal_height!' => 'yes',
        )
    )
);

$widget->add_control(
    'products_shadow',
    array(
        'label' => esc_html__('Show shadow on hover?', 'gt3_themes_core'),
        'type'  => Controls_Manager::SWITCHER,
    )
);

$widget->add_control(
    'products_shadow_color',
    array(
        'label'     => esc_html__('Shadow Color', 'gt3_themes_core'),
        'type'      => Controls_Manager::COLOR,
        'default'   => 'rgba(239,239,239,1)',
        'selectors' => array(
            '{{WRAPPER}} .woocommerce ul.products.shadow li.product:hover:before'      => 'color: {{COLOR}};',
            '.woocommerce-page {{WRAPPER}} ul.products.shadow li.product:hover:before' => 'color: {{COLOR}};',
        ),
        'condition' => array(
            'products_shadow' => 'yes',
        )
    )
);

$widget->add_control(
    'prod_bgcolor_1',
    array(
        'label'       => esc_html__('Background Color', 'gt3_themes_core'),
        'description' => esc_html__('Select the Background Color for each Products', 'gt3_themes_core'),
        'type'        => Controls_Manager::COLOR,
        'default'     => 'transparent',
    )
);

$widget->add_control(
    'prod_bgcolor_2',
    array(
        'label'       => esc_html__('Hover Background Color', 'gt3_themes_core'),
        'description' => esc_html__('Select the Background Color for each Products in hover', 'gt3_themes_core'),
        'type'        => Controls_Manager::COLOR,
        'default'     => 'transparent',
    )
);

$widget->add_control(
    'scroll_anim',
    array(
        'label' => esc_html__('Use Scroll Animation?', 'gt3_themes_core'),
        'type'  => Controls_Manager::SWITCHER,
    )
);

$widget->add_control(
    'shop_grid_list',
    array(
        'label'       => esc_html__('Show Grid/List toggle Buttons', 'gt3_themes_core'),
        'type'        => Controls_Manager::SWITCHER,
        'condition'   => array(
            'grid_style' => array(
                'grid_default hover_default',
                'grid_default hover_bottom',
                'grid_default hover_center',
                'grid_default_woo hover_default',
                'grid_default_woo hover_bottom',
                'grid_default_woo hover_center'
            ),
        )
    )
);

$widget->end_controls_section();

$widget->start_controls_section(
    'buttons',
    array(
        'label' => esc_html__('Button Layout', 'gt3_themes_core'),
        'type'  => Controls_Manager::SECTION,
        'tab'   => Controls_Manager::TAB_STYLE,
    )
);

$widget->add_control(
    'cart_btn',
    array(
        'label'   => esc_html__('Add to Cart button position', 'gt3_themes_core'),
        'type'    => Controls_Manager::SELECT,
        'options' => array(
            'top'    => esc_html__('Top and Right', 'gt3_themes_core'),
            'middle' => esc_html__('Middle', 'gt3_themes_core'),
            'bottom' => esc_html__('Bottom', 'gt3_themes_core'),
        ),
        'default' => 'bottom',
    )
);

if (class_exists('YITH_WCWL_Shortcode') && get_option('yith_wcwl_enabled') == true) {
    $widget->add_control(
        'wishlist_btn',
        array(
            'label'       => esc_html__('Wishlist button position', 'gt3_themes_core'),
            'description' => esc_html__('This option works if "YITH WooCommerce Wishlist" plugin is installed and active.', 'gt3_themes_core'),
            'type'        => Controls_Manager::SELECT,
            'options'     => array(
                'top'    => esc_html__('Top and Right', 'gt3_themes_core'),
                'middle' => esc_html__('Middle', 'gt3_themes_core'),
            ),
            'default'     => 'top',
        )
    );
}

if (class_exists('YITH_WCQV_Frontend') && get_option('yith-wcqv-enable')) {
    $widget->add_control(
        'quick_view_btn',
        array(
            'label'       => esc_html__('Quick View button position', 'gt3_themes_core'),
            'description' => esc_html__('This option works if "YITH WooCommerce Quick View" plugin is installed and active.', 'gt3_themes_core'),
            'type'        => Controls_Manager::SELECT,
            'options'     => array(
                'top'    => esc_html__('Top and Right', 'gt3_themes_core'),
                'middle' => esc_html__('Middle', 'gt3_themes_core'),
            ),
            'default'     => 'top',
        )
    );
}

if (class_exists('YITH_Woocompare')) {
    $widget->add_control(
        'compare_btn',
        array(
            'label'       => esc_html__('Compare button position', 'gt3_themes_core'),
            'description' => esc_html__('This option works if "YITH WooCommerce Compare" plugin is installed and active.', 'gt3_themes_core'),
            'type'        => Controls_Manager::SELECT,
            'options'     => array(
                'top'    => esc_html__('Top and Right', 'gt3_themes_core'),
                'middle' => esc_html__('Middle', 'gt3_themes_core'),
            ),
            'default'     => 'top',
        )
    );
}

$widget->add_control(
    'dropdown_prod_per_page',
    array(
        'label'       => esc_html__('Dropdown on Frontend - Items Per Page', 'gt3_themes_core'),
        'description' => esc_html__('Show the dropdown to change the Number of products displayed per page', 'gt3_themes_core'),
        'type'        => Controls_Manager::SELECT,
        'options'     => array(
            'top'        => esc_html__('Top', 'gt3_themes_core'),
            'bottom'     => esc_html__('Bottom', 'gt3_themes_core'),
            'bottom_top' => esc_html__('Bottom and Top', 'gt3_themes_core'),
            'off'        => esc_html__('Off', 'gt3_themes_core'),
        ),
        'default'     => 'bottom_top',
        'condition'   => array(
            'infinity_scroll!' => '1',
        )
    )
);

$widget->add_control(
    'dropdown_prod_orderby',
    array(
        'label'       => esc_html__('Dropdown on Frontend - Order by', 'gt3_themes_core'),
        'description' => esc_html__('Show the dropdown to change the Sorting of products displayed per page', 'gt3_themes_core'),
        'type'        => Controls_Manager::SELECT,
        'options'     => array(
            'top'        => esc_html__('Top', 'gt3_themes_core'),
            'bottom'     => esc_html__('Bottom', 'gt3_themes_core'),
            'bottom_top' => esc_html__('Bottom and Top', 'gt3_themes_core'),
            'off'        => esc_html__('Off', 'gt3_themes_core'),
        ),
        'default'     => 'bottom_top',
    )
);

$widget->add_control(
    'pagination',
    array(
        'label'       => esc_html__('Pagination on Frontend', 'gt3_themes_core'),
        'description' => esc_html__('Show Pagination on the page', 'gt3_themes_core'),
        'type'        => Controls_Manager::SELECT,
        'options'     => array(
            'top'        => esc_html__('Top', 'gt3_themes_core'),
            'bottom'     => esc_html__('Bottom', 'gt3_themes_core'),
            'bottom_top' => esc_html__('Bottom and Top', 'gt3_themes_core'),
            'off'        => esc_html__('Off', 'gt3_themes_core'),
        ),
        'default'     => 'bottom_top',
        'condition'   => array(
            'infinity_scroll!' => '1',
        )
    )
);

$widget->end_controls_section();